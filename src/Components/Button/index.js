import React from 'react';
const Button = (props) => {
    const { name, type, cssClass, style, disabled, onClick } = props;
    return (
        <button 
            onClick={ () => onClick()} 
            style={style} 
            className={`btn btn-secondary ${cssClass}`} 
            disabled={disabled} 
            type={type}>{name}
        </button>
    )
}
export default Button;