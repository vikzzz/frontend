import React from 'react';
const TextArea = ( props ) => {
    const { value, readOnly, placeholder, onChange, required, name, showValue } = props;
    return (
        <textarea name={name} required={required} style={{'width': '350px'}} disabled={readOnly} placeholder={placeholder} onChange={onChange } defaultValue={showValue ? value : ''}></textarea>
    )
}
export default TextArea;