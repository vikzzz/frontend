import React from 'react';
const TextBox = ( props ) => {
    const { value, readOnly, name, type, placeholder, style, required, onChange } = props;
    return (
        <input type={type} name={name} value={value} readOnly={readOnly} style={style} placeholder={placeholder} onChange={onChange} required={required} />
    )
}
export default TextBox;