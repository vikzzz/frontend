import React from 'react';
import { Navbar } from 'react-bootstrap';

function Header() {

    return (
        <Navbar bg="light" expand="lg" fixed="top" id="main-navbar">
            <Navbar.Brand href="/">
                Ice Queen
            </Navbar.Brand>
            <div className="d-none d-md-inline-block">User Avatar</div>
        </Navbar>
    );
}

export default Header;
