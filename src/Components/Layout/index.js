import React from 'react';
import { Container, Row } from 'react-bootstrap';
import Header from './header';

const Layout = ( props ) => {
    return(
        <React.Fragment>
        <Header/>
        <Container className="container" id="layout-container">
            <Row>
                <div className={`content_area w-100`}>
                    <div className='w-100' id="wrapper">
                        {props.children}
                    </div>
                </div>
            </Row>
        </Container>
        </React.Fragment>
    )
}
export default Layout;