import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { saveFlavourAction } from '../../Pages/IceQueen/actions';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TextBox from '../../Components/TextBox';
import TextArea from '../../Components/TextArea';

const AddFlavour = ( props ) => {

    const dispatch = useDispatch();
    
    const flavourState  = useSelector(state => state.flavourReducer);

    
    const handleChange = ( event ) => {
        setValue(prevState => {
            return {...prevState, [event.target.name]: event.target.value };
        });
    }

    const handleArrValues = ( event, i ) => {
        setValue(prevState => {
            const name = event.target.name;
            let prevData = prevState[name];
            prevData[i] = event.target.value;
            return {...prevState, [name]: prevData };
        });
    }

    const textInputStyle = {'width': '110px'}
    const initialState = {
        'flavourName': '',
        'description': '',
        'size':[
            'Small',
            'Medium',
            'Large'
        ],
        'quantity':[
            '',
            '',
            ''
        ],
        'price':[
            '',
            '',
            ''
        ]
    };
    const [value, setValue] = useState(initialState);

    const handleSubmit = ( e ) => {
        e.preventDefault();
        dispatch(saveFlavourAction(value));
        setValue(prevState => {
            return {...prevState, ...initialState };
        });
        e.target.reset();
    }

    console.log(value,"This is the state.......");

    const notify = ( message , id  ) => {
        toast.dismiss();
        id === "success" ? 
        toast.success(message,{
            position: "top-right",
            autoClose: 20000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            toastId:id
        }) : toast.warning(message,{
            position: "top-right",
            autoClose: 10000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            toastId:id
        })
    }

    return(
        <>
            {props.show ? <React.Fragment>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            {flavourState.saveFlavour.result !== null && notify("Flavor Saved Successfully!","success")}
            {flavourState.saveFlavour.error !== null && notify("Something going wrong. Please try again Later!","warning")}
                
                <div className="col-md-4 mt-5 detailedColumn">
                    <div className="content">
                        <div className="row">
                            <form name="add-flavour" method="post" onSubmit={ (e) => handleSubmit(e) }>
                                <div className="col-md-12">
                                    <h4>Add New Flavour</h4>
                                    <div className="form-group">
                                        <TextBox style={{"width":"100%"}} value={value.flavourName} onChange={ ( e ) => handleChange( e ) } readOnly={false} name="flavourName" type="texbox" placeholder="Enter Flavour Name" required />
                                    </div>
                                    <div className="form-group">
                                        <TextArea name="description" value={value.description || ""} onChange={ ( e ) => handleChange( e ) }></TextArea>
                                    </div>
                                    {Array.from(Array(3), ( e, i) => {
                                        let size = "Small";
                                        if( i === 0 ){
                                            size = "Small";
                                        }else if( i === 1 ){
                                            size = "Medium";
                                        }else{
                                            size = "Large";
                                        }
                                        return (
                                            <div key={`main--${i}`} className="form-group addFormflavours">
                                                <TextBox key={`size-${i}`} value={value.size[i]} onChange={ ( e ) => handleArrValues( e, i ) } readOnly={true} name={`size`} type="texbox" placeholder="Small" style={textInputStyle} />
                                                <TextBox key={`quantity-${i}`}  value={value?.quantity[i]} onChange={ ( e ) => handleArrValues( e, i ) } readOnly={false} name={`quantity`} type="number" min="1" pattern="\d+" placeholder="Quantity" style={textInputStyle}/>
                                                <TextBox key={`price-${i}`}  value={value?.price[i] } onChange={ ( e ) => handleArrValues( e, i ) } readOnly={false} name={`price`} type="number" min="1" pattern="\d+" placeholder="Price" style={textInputStyle}/>
                                            </div>
                                        )
                                    })}
                                </div>
                                <div className="col-md-12 alignItems">
                                    <button type="submit" name="close" className="btn-secondary">Add Flavour</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </React.Fragment> : null }
        </>
    )
}
export default AddFlavour;