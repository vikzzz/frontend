import React from 'react';
import FlavourImg from '../../assets/flavour.png';
const Flavours = ( props ) => {
    const { item }  = props;
    return (
        <>
            {item && item.name ? <div className="flavourObj"><div className="flavourImg">
                <img src={FlavourImg} alt="flavourImg" />
            </div>
            <p>{item.name}</p></div>: null}
        </>
    )
}
export default Flavours;