import React from 'react';
import SpinnerImg from '../../assets/spinner.gif';
export const Spinner = ( props ) => {
    return (
        props.loading ? <div className="spinnerImg">
            <img src={SpinnerImg} alt="Ice Queen" />
        </div> : <></>
    )
}