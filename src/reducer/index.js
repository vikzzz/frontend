import { combineReducers } from "redux";
import { flavourReducer } from '../Pages/IceQueen/reducer';
const createRootReducer = (history) => combineReducers({
    flavourReducer
});
export default createRootReducer