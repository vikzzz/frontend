import { Constants } from "./constants";
let domainUrl = Constants.BASE_URL;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  GET: (link, data) =>
    new Promise((resolve, reject) => {
      const url = domainUrl + link;
      console.log(url, "This is url.............");
      fetch(url, {
        method: "GET",
      })
        .then(response => response.json())
        .then(responseText => {
          resolve(responseText);
        })
        .catch(error => {
          reject(error);
        });
    }),

  POST: (link, data) =>
    new Promise((resolve, reject) => {
      const url = domainUrl + link;
      console.log(url, "This is url.............");
      fetch(url, {
        body: JSON.stringify(data),
        method: "POST",
        headers: {
          "Content-type": "application/json"
        }
      })
        .then(response => response.json())
        .then(responseText => {
          resolve(responseText);
        })
        .catch(error => {
          reject(error);
        });
    })
};