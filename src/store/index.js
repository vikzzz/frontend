import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
//import RootReducer from '../reducer';
import createRootReducer from '../reducer'
import thunk from 'redux-thunk'
import CreateSagaMiddleware from 'redux-saga';
import { rootSaga } from '../saga/index';

const sagaMiddleWare = CreateSagaMiddleware();
const middleware = [  thunk, sagaMiddleWare ];

if (process.env.NODE_ENV !== 'production') {
  const logger = createLogger();
  middleware.push(logger);
}

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      ...middleware        
    )
  )
  return createStore( 
    createRootReducer(), 
        initialState, 
        enhancer
    )
}
const store = configureStore({})
sagaMiddleWare.run(rootSaga)



export default store;
