import React, { useEffect } from 'react';
import Layout from '../../Components/Layout';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { useDispatch, useSelector } from 'react-redux';
import { flavourFetchAction, singleFlavourFetchAction, singleFlavourResponseEmpty } from './actions';
import { Spinner } from '../../Components/Spinner';
import { Row } from 'react-bootstrap';
import Flavours from '../../Components/Flavours';
import Button from '../../Components/Button';
import TextBox from '../../Components/TextBox';
import TextArea from '../../Components/TextArea';
import AddFlavour from '../../Components/Flavours/AddFlavour';

const IceQueen = () => {

    const dispatch = useDispatch();

    useEffect( () => {
        dispatch(flavourFetchAction());
    },[dispatch]);

    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 6
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 6
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 3
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 3
        }
    };
    const flavourState  = useSelector(state => state.flavourReducer);
    const flavours = flavourState.flavours.result;
    const singleFlavour = flavourState.singleFlavor;
    
    let result = {}

    if( singleFlavour && singleFlavour.result && singleFlavour.result !== null ){
        Object.assign(result,{name:singleFlavour.result.name,id:singleFlavour.result.id, description:singleFlavour.result.description, data: singleFlavour.result.data})
    }
    const flavourDetail = (id) => {
        dispatch(singleFlavourFetchAction(id));
    }
    const closeButtonClick = () => {
        dispatch(singleFlavourResponseEmpty());
    }

    const textInputStyle = {'width': '110px'}

    const addNewFlavour = () => {
        dispatch(singleFlavourResponseEmpty());
    }

    return(
        <Layout>
            {flavourState.flavours.pending && <Spinner loading={flavourState.flavours.pending} />}
            <h2 className="w-100 align-items">Flavours</h2>
            <Row>
                <div className="col-md-8">
                    <section className="carousel-container content-block block p-2 mt-5">
                        <Carousel 
                            responsive={responsive}
                            ssr={true}
                        >
                        {flavours !== null && flavours.length > 0 ? flavours.map( item => {
                                return <div key={item.id} className="courourselItmes" onClick={ () => flavourDetail(item.id) }>
                                    <Flavours item={item}/>
                                </div>
                            }) : flavourState.pending === false ? <h2 className="w-100 align-items">No Item Found</h2> : <></> }
                        </Carousel>
                    </section>
                    <Button type="button" className="btn-secondary" style={{'float':'right','marginRight': '45px'}} name="Add New" onClick={ () => addNewFlavour() }></Button>
                </div>
                {Object.keys(result).length > 0 && <div className="col-md-4 mt-5 detailedColumn">
                    <div className="content">
                        <Flavours item={result}/>
                        <TextArea value={result.description} showValue={true} readOnly={true}></TextArea>
                        <div className="row">
                            {result.data && result.data.map(( r, i ) => {
                                return (
                                    <div key={`detailedData-${i}`} className="detailedData">
                                        <TextBox key={`type-${i}`} value={r.flavourSize+": "+r.price+" $" } readOnly={true} name="texbox" type="texbox" placeholder="Small" style={textInputStyle} />
                                        <TextBox key={`quantity-${i}`} value={`Quantity: ${r.quantity}`} readOnly={true} name="texbox" type="text" placeholder="Quantity" style={textInputStyle}/>
                                        <TextBox key={`sold-${i}`} value={`Sold: ${r.soldQuanity}`} readOnly={true} name="texbox" type="text" placeholder="Sell" style={textInputStyle}/>
                                    </div>
                                )
                            })}
                            <div className="col-md-12 alignItems">
                                <Button name="close" cssclassName="btn-secondary" onClick={ () => closeButtonClick() } />
                            </div>
                        </div>
                    </div>
                </div>}
                <AddFlavour show={Object.keys(result).length === 0 ? true : false}/>
            </Row>
        </Layout>
    )
}
export default IceQueen;