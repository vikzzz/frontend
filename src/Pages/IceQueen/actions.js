import {
    FLAVOUR_FETCH_ACTION,
    FLAVOUR_FETCH_PENDING,
    FLAVOUR_FETCH_SUCCESS,
    FLAVOUR_FETCH_ERROR,
    SINGLE_FLAVOUR_FETCH_ACTION,
    SINGLE_FLAVOUR_FETCH_PENDING,
    SINGLE_FLAVOUR_FETCH_SUCCESS,
    SINGLE_FLAVOUR_FETCH_ERROR,
    SINGLE_FLAVOUR_EMPTY_RESPONSE,
    SAVE_FLAVOUR_ACTION,
    SAVE_FLAVOUR_PENDING,
    SAVE_FLAVOUR_SUCCESS,
    SAVE_FLAVOUR_ERROR,
    RESET_SAVE_FLAVOUR_STATE
} from "./type";

export const saveFlavourAction = ( data ) => {
    return {
        type: SAVE_FLAVOUR_ACTION,
        data
    }
}

export const saveFlavourPending = ( pending ) => {
    return {
        type: SAVE_FLAVOUR_PENDING,
        pending
    }
}

export const saveFlavourSuccess = ( result, pending ) => {
    return {
        type: SAVE_FLAVOUR_SUCCESS,
        pending,
        result
    }
}

export const resetSaveFlavourState = () => {
    return {
        type: RESET_SAVE_FLAVOUR_STATE
    }
}

export const saveFlavourError = ( error, pending ) => {
    return {
        type: SAVE_FLAVOUR_ERROR,
        pending,
        error
    }
}

export const singleFlavourResponseEmpty = () => {
    return {
        type: SINGLE_FLAVOUR_EMPTY_RESPONSE
    }
}

export const flavourFetchAction = () => {
    return {
        type: FLAVOUR_FETCH_ACTION
    }
};

export const flavourFetchPending = (pending) => {
    return {
        type: FLAVOUR_FETCH_PENDING,
        pending
    }
};

export const flavourFetchError = (error, pending) => {
    return {
        type: FLAVOUR_FETCH_ERROR,
        error,
        pending
    }
};

export const flavourFetchSuccess = (result, pending) => {
    return {
        type: FLAVOUR_FETCH_SUCCESS,
        result,
        pending
    }
};

export const singleFlavourFetchAction = (id) => {
    return {
        type: SINGLE_FLAVOUR_FETCH_ACTION,
        id
    }
};

export const singleFlavourFetchPending = (pending) => {
    return {
        type: SINGLE_FLAVOUR_FETCH_PENDING,
        pending
    }
};

export const singleFlavourFetchError = (error, pending) => {
    return {
        type: SINGLE_FLAVOUR_FETCH_ERROR,
        error,
        pending
    }
};

export const singleFlavourFetchSuccess = (result, pending) => {
    return {
        type: SINGLE_FLAVOUR_FETCH_SUCCESS,
        result,
        pending
    }
};