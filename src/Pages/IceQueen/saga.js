import { put, takeLatest, call } from 'redux-saga/effects';
import * as type from './type';
import * as actions from './actions';
import Api from './api';

function* fetchFlavours( action){
    yield put( actions.flavourFetchPending(true) );
    try{
        const response = yield call( Api.flavoursApi );
        if( response.success === 1 ){
            yield put( actions.flavourFetchSuccess( response.data, false ) );
        }else{
            yield put( actions.flavourFetchError( response.message, false ) );
        }
    }catch( e ){
        yield put( actions.flavourFetchError(e.toString(), false) );
    }
    yield put( actions.resetSaveFlavourState() );
}

function* fetchSingleFlavour( action){
    yield put( actions.singleFlavourFetchPending(true) );
    try{
        const response = yield call( Api.singleFlavourApi, action.id );
        if( response.success === 1 ){
            yield put( actions.singleFlavourFetchSuccess( response.data, false ) );
        }else{
            yield put( actions.singleFlavourFetchError( response.message, false ) );
        }
    }catch( e ){
        yield put( actions.singleFlavourFetchError(e.toString(), false) );
    }
}

function* saveFlavour( action){
    yield put( actions.saveFlavourPending(true) );
    try{
        const response = yield call( Api.saveFlavourApi, action.data );
        if( response.success === 1 ){
            yield put( actions.saveFlavourSuccess( response.data, false ) );
            yield put(actions.flavourFetchAction());
        }else{
            yield put( actions.saveFlavourError( response.message, false ) );
        }
    }catch( e ){
        yield put( actions.saveFlavourError(e.toString(), false) );
    }
}

function* watchLoginSaga() {
    yield takeLatest( type.FLAVOUR_FETCH_ACTION, fetchFlavours );
    yield takeLatest( type.SINGLE_FLAVOUR_FETCH_ACTION, fetchSingleFlavour );
    yield takeLatest( type.SAVE_FLAVOUR_ACTION, saveFlavour );
}

export default watchLoginSaga;
