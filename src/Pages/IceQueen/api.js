import coreApi from "../../lib/coreApi";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    flavoursApi: () => {
        const url = `/api/flavours`;
        const result = coreApi.GET( url );
        return result;
    },
    singleFlavourApi: ( id ) => {
        const url = `/api/flavour/${id}`;
        const result = coreApi.GET( url );
        return result;
    },
    saveFlavourApi: ( data ) => {
        const url = `/api/flavour`;
        const result = coreApi.POST( url, data );
        return result;
    },
};
