import {
    FLAVOUR_FETCH_PENDING,
    FLAVOUR_FETCH_SUCCESS,
    FLAVOUR_FETCH_ERROR,
    SINGLE_FLAVOUR_FETCH_PENDING,
    SINGLE_FLAVOUR_FETCH_SUCCESS,
    SINGLE_FLAVOUR_FETCH_ERROR,
    SINGLE_FLAVOUR_EMPTY_RESPONSE,
    SAVE_FLAVOUR_PENDING,
    SAVE_FLAVOUR_SUCCESS,
    SAVE_FLAVOUR_ERROR,
    RESET_SAVE_FLAVOUR_STATE
} from "./type";

const initialState = {
    flavours: {
        pending: false,
        error: "",
        result: null
    },
    singleFlavor: {
        pending: false,
        error: "",
        result: null
    },
    saveFlavour: {
        pending: false,
        error: null,
        result: null
    }
};

function flavourState(
    state = initialState,
    action
) {
    switch (action.type) {
        case RESET_SAVE_FLAVOUR_STATE:{
            return {
                ...state,
                saveFlavour: {
                    ...state.saveFlavour,
                    pending: false,
                    error: null,
                    result: null
                }
            }
        }
        case SAVE_FLAVOUR_PENDING: {
            return {
                ...state,
                saveFlavour: {
                    ...state.saveFlavour,
                    pending: action.pending
                }
            }
        }
        case SAVE_FLAVOUR_SUCCESS: {
            return {
                ...state,
                saveFlavour: {
                    ...state.saveFlavour,
                    pending: action.pending,
                    result: action.result
                }
            }
        }
        case SAVE_FLAVOUR_ERROR: {
            return {
                ...state,
                saveFlavour: {
                    ...state.saveFlavour,
                    pending: action.pending,
                    error: action.error,
                }
            }
        }
        case FLAVOUR_FETCH_PENDING: {
            return {
                ...state,
                flavours: {
                    ...state.flavours,
                    pending: action.pending
                }
            }
        }
        case FLAVOUR_FETCH_SUCCESS: {
            return {
                ...state,
                flavours: {
                    ...state.flavours,
                    pending: action.pending,
                    result: action.result
                }
            }
        }
        case FLAVOUR_FETCH_ERROR: {
            return {
                ...state,
                flavours: {
                    ...state.flavours,
                    pending: action.pending,
                    error: action.error,
                }
            }
        }
        case SINGLE_FLAVOUR_FETCH_PENDING: {
            return {
                ...state,
                singleFlavor: {
                    ...state.singleFlavor,
                    pending: action.pending
                }
            }
        }
        case SINGLE_FLAVOUR_FETCH_SUCCESS: {
            return {
                ...state,
                singleFlavor: {
                    ...state.singleFlavor,
                    pending: action.pending,
                    result: action.result
                }
            }
        }
        case SINGLE_FLAVOUR_FETCH_ERROR: {
            return {
                ...state,
                singleFlavor: {
                    ...state.singleFlavor,
                    pending: action.pending,
                    error: action.error,
                }
            }
        }
        case SINGLE_FLAVOUR_EMPTY_RESPONSE: {
            return {
                ...state,
                singleFlavor: {
                    ...state.singleFlavor,
                    result: null
                }
            }
        }
        default:
            return state;
    }
}

export const flavourReducer = flavourState;
