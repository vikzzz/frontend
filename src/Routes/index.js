import React from 'react';
import {
        BrowserRouter as Router,
        Switch,
        Route,
        Link
} from "react-router-dom";
import IceQueen from '../Pages/IceQueen';
export const Routes = () => (
    <Switch>
        <Route path="/">
            <IceQueen />
        </Route>
    </Switch>
)