import { fork } from 'redux-saga/effects';
import watchIceQueenSaga from '../Pages/IceQueen/saga';
export function* rootSaga(){
  yield fork(watchIceQueenSaga);
}
